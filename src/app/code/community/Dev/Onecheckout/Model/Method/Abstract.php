<?php

abstract class Dev_onecheckout_Model_Method_Abstract extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'onecheckout_abstract';

    protected $_formBlockType = 'onecheckout/payment_form';
    // protected $_infoBlockType = 'onecheckout/payment_info';

    /**
     * Availability options
     */
    protected $_isGateway              = true;
    protected $_canAuthorize           = true;
    protected $_canCapture             = true;
    protected $_canCapturePartial      = false;
    protected $_canRefund              = false;
    protected $_canVoid                = false;
    protected $_canUseInternal         = false;
    protected $_canUseCheckout         = true;
    protected $_canUseForMultishipping = false;

}
