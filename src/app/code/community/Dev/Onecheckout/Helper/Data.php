<?php

class Dev_Onecheckout_Helper_Data extends Mage_Payment_Helper_Data
{
    /**
     * Retrieve quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }

    /**
     * get date of birth
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return string
     */
    public function getDoB($quote)
    {
        $isGuest = $quote->getCustomerIsGuest();

        if (!$isGuest) {
            $dob = $quote->getCustomerDob();
        }else{
            $dob = Mage::getSingleton('customer/session')->getCustomer()->getDob();
        }

        return $dob;
    }

    /**
     * Retrieve billing address data
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getBillingAddress($quote)
    {
        $billingAddresses = array(
            'first_name'   => $quote->getBillingAddress()->getFirstname(),
            'last_name'    => $quote->getBillingAddress()->getLastname(),
            'company'      => $quote->getBillingAddress()->getCompany(),
            'phone'        => $quote->getBillingAddress()->getTelephone(),
            'street'       => str_replace("\n", " ", $quote->getBillingAddress()->getStreetFull()),
            'vat'          => $quote->getBillingAddress()->getVatId(),
            'city'         => $quote->getBillingAddress()->getCity(),
            'state'        => $quote->getBillingAddress()->getRegion(),
            'zip'          => $quote->getBillingAddress()->getPostcode(),
            'country_code' => $quote->getBillingAddress()->getCountryId(),
        );

        return $billingAddresses;
    }

    /**
     * Retrieve shipping address data
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getShippingAddress($quote)
    {
        $shipingAddresses = array(
            'first_name'   => $quote->getShippingAddress()->getFirstname(),
            'last_name'    => $quote->getShippingAddress()->getLastname(),
            'company'      => $quote->getShippingAddress()->getCompany(),
            'phone'        => $quote->getShippingAddress()->getTelephone(),
            'street'       => str_replace("\n", " ", $quote->getShippingAddress()->getStreetFull()),
            'vat'          => $quote->getShippingAddress()->getVatId(),
            'city'         => $quote->getShippingAddress()->getCity(),
            'state'        => $quote->getShippingAddress()->getRegion(),
            'zip'          => $quote->getShippingAddress()->getPostcode(),
            'country_code' => $quote->getShippingAddress()->getCountryId(),
        );

        return $shipingAddresses;
    }

    /**
     * Retrieve contact data
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getContactData($quote)
    {
        $data = array(
            'email' => $quote->getCustomerEmail(),
            'phone' => $quote->getBillingAddress()->getTelephone()
        );

        return $data;
    }

    /**
     * Retrieve Basket data
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return array
     */
    public function getBasketData($order)
    {
        $basket = array(
            'amount' => $order->getGrandTotal(),
            'baseCurrency' => $order->getBaseCurrencyCode(),
            'baseAmount' => $order->getBaseGrandTotal(),
            'currency' => $order->getQuoteCurrencyCode(),
            'shippingMethod' => $order->getShippingAddress()->getShippingMethod(),
            'shippingDescription' => $order->getShippingAddress()->getShippingDescription(),
            'shippingAmount' => $order->getShippingAddress()->getShippingAmount(),
            'shippingInclTax' => $order->getShippingAddress()->getShippingInclTax(),
            'shippingDiscountAmount' => $order->getShippingAddress()->getShippingDiscountAmount(),
            'shippingTaxAmount' => $order->getShippingAddress()->getShippingTaxAmount(),
        );
        return $basket;
    }

    /**
     * This method returns the customer gender
     *
     * @param Mage_Sales_Model_Quote $quote
     * @return string
     */
    public function getGender($quote)
    {
        $gender = $quote->getCustomerGender();

        switch ($gender) {
            case '1':
                return 'M';
            case '2':
                return 'F';
        }
        return false;
    }

    /**
     * Retrieve the locale code in iso (2 chars)
     *
     * @return string
     */
    public function getLocaleIsoCode()
    {
        return substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2);
    }

    /**
     * Retrieve the customer ip
     *
     * @return string
     */
    public function getCustomerIp()
    {
        if (Mage::helper('core/http')->getRemoteAddr() == '::1'){
            return '127.0.0.1';
        }else{
            return Mage::helper('core/http')->getRemoteAddr();
        }
    }

}
